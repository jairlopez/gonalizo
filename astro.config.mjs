import { defineConfig } from 'astro/config';

import tailwind from "@astrojs/tailwind";

import mdx from "@astrojs/mdx";
import rehypePrettyCode from "rehype-pretty-code";

import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
    site: 'https://www.gonalizo.com',
    markdown: {
        syntaxHighlight: false,
        shikiConfig: { theme: 'dracula' },
        remarkRehype: { footnoteLabel: 'Footnotes' },
        rehypePlugins: [rehypePrettyCode],
    },
    integrations: [tailwind(), mdx(), sitemap()],
    i18n: {
        defaultLocale: "en",
        locales: ["es", "en"]
    }
});
