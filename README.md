# Gonalizo

My personal website made with Astro.

# License

This portfolio website is open-sourced software licensed under the [MIT
license](https://opensource.org/licenses/MIT).
