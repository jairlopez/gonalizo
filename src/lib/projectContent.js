import { getFilteredCollection } from './filteredCollection'
import { generateGetStaticPathsForSinglePage } from "./blogContent"
import { useTranslatedProjectsPath } from "./i18n/utils.js"

export async function getProjectItems(language) {
    return await getFilteredCollection('projects', language, true);
}

export function generateGetStaticPathsForSingleProjectPage(language) {
    const projectPath = useTranslatedProjectsPath(language);

    return generateGetStaticPathsForSinglePage(language, 'projects', projectPath);
}
