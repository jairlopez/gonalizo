import rss from "@astrojs/rss";
import { getLangFromUrl, useTranslations, useTranslatedBlogPath } from './i18n/utils';
import { getFilteredCollection } from './filteredCollection';

export async function generateGetFunction() {
    return async function (context) {
        const language = await getLangFromUrl(context.url);
        const t = await useTranslations(language);
        const blogPath = useTranslatedBlogPath(language);
        const blog = await getFilteredCollection('blog', language, true);

        return rss({
            title: t('main.site-title'),
            description: t('main.site-description'),
            site: import.meta.env.SITE,
            items: blog.map((post) => ({
                title: post.data.title,
                pubDate: post.data.pubDate,
                description: post.data.description,
                link: blogPath({title: post.data.title, id: post.id})
            })),
        });
    };
}
