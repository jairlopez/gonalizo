import { getLangFromUrl, canonicalBlogTagUrl, useTranslatedTagPath, useTranslatedBlogPath, languages } from '../lib/i18n/utils';
import createSlug from "./createSlug"
import { getFilteredCollection } from "./filteredCollection"

export function generateGetStaticPathsForSinglePage(language, collectionName, path) {
    return async function () {
        const translations = {};

        const filteredPosts = await getFilteredCollection(collectionName, language, false, entry => {
            const id = entry.id.replace(/^.+?\//, '');
            const lang = entry.id.replace(/\/.*/, '');

            translations[id] ??= {};

            translations[id][lang] = path({title: entry.data.title, id: entry.id}, lang);
        });

        for (const entry of filteredPosts) {
            const id = entry.id.replace(/^.+?\//, '');

            entry.data.translations = translations[id] ?? {};
        }

        return filteredPosts
            .map((entry) => ({
                params: { slug: createSlug(entry.data.title, entry.id) },
                props: { entry },
            }));
    };
}

export function generateGetStaticPathsForSingleBlogPage(language) {
    const blogPath = useTranslatedBlogPath(language);

    return generateGetStaticPathsForSinglePage(language, 'blog', blogPath);
}

export function generateGetStaticPathsForListing(language) {
    return async function ({ paginate }) {
        return paginate(
            await getFilteredCollection('blog', language, true),
            { pageSize: 10 }
        );
    };
}

export function generateGetStaticPathsFunctionForTag(language) {
    const path = useTranslatedTagPath(language);

    let tagDirName = path('');
    tagDirName = tagDirName.split('/')[tagDirName === canonicalBlogTagUrl ? 2 : 3];

    return async function ({ paginate }) {
        const all_posts = await getFilteredCollection('blog', language);

        const all_tags = all_posts.flatMap((post) => {
            return post.data.tags || [];
        });

        return all_tags.flatMap((tag) => {
            const filtred_posts = all_posts.filter((post) => {
                return post.data.tags?.includes(tag);
            });

            const params = {};
            params[tagDirName] = path(tag).split('/').splice(-1)[0];

            const translations = {};

            for (const langItem in languages) {
                if (langItem !== language) {
                    translations[langItem] = path(tag, langItem);
                }
            }

            return paginate(filtred_posts, {
                params,
                pageSize: 10,
                props: {
                    tagDirName,
                    translations,
                    canonicalTag: tag
                }
            });
        });
    };
}
