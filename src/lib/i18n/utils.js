import { getCollection } from 'astro:content';
import createSlug from '../createSlug';

export const canonicalProjectsUrl = '/projects';
export const canonicalBlogUrl = '/blog';
export const canonicalBlogTagUrl = '/blog/tag';
export const defaultLang = 'en';

export const languages = {
    en: 'English',
    es: 'Español',
};

export const routes = {
    es: {
        // Translatable tags.
        '/blog/tag/one': '/blog/etiqueta/uno',
        '/blog/tag/two': '/blog/etiqueta/dos',
        '/blog/tag/three': '/blog/etiqueta/tres',
        '/blog/tag/book-review': '/blog/etiqueta/resena-de-libros',

        '/blog/tag': '/blog/etiqueta',
        '/projects': '/proyectos',
        '/resume': '/curriculum',
        '/about': '/acerca-de-mi',
    }
};


let translations;

export async function getLangFromUrl(url) {
    const [, lang] = url.pathname.split('/');

    if (!translations) {
        translations = await loadTranslations();
    }

    if (lang in translations) {
        return lang;
    }

    return defaultLang;
}

async function loadTranslations() {
    const collection = await getCollection('translations');

    let translations = {};

    collection.map((entry) => {
        const [lang] = entry.id.split('/');

        translations[lang] = entry.data;
    });

    return translations;
}

export async function useTranslations(language) {
    if (!translations) {
        translations = await loadTranslations();
    }

    return function (key, lang = language) {
        return translations[lang][key] || translations[defaultLang][key] || '';
    };
}

export async function useTagTranslations(language) {
    const t = await useTranslations(language);

    return function (canonicalTag, lang = language) {
        return t(`tagname.${canonicalTag}`, lang) || canonicalTag;
    };
}

export function useTranslatedPath(lang) {
    return function(path, language = lang) {
        const hasTranslation = defaultLang !== language && routes[language] !== undefined && routes[language][path] !== undefined;
        const translatedPath = hasTranslation ? routes[language][path] : path;

        if (defaultLang === language) {
            return translatedPath;
        }

        return `/${language}` + (translatedPath.replace(/\/+$/, ''));
    };
}


export function useTranslatedTagPath(lang) {
    const path = useTranslatedPath(lang);

    return function(tagName, language = lang) {
        tagName = tagName ? `/${tagName}` : '';

        const url = canonicalBlogTagUrl + tagName;


        if (defaultLang === language) {
            return url;
        } else {
            const hasTranslation = defaultLang !== language && routes[language] !== undefined && routes[language][url] !== undefined;
            const translatedTagPath = hasTranslation ? routes[language][url] : routes[language][canonicalBlogTagUrl] + tagName;

            return `/${language}` + translatedTagPath.replace(/\/+$/, '');
        }
    };
}

function useTranslatedCanonicalPath(lang, canonicalPath) {
    const path = useTranslatedPath(lang);

    return function(entry, language = lang) {
        let url = path(canonicalPath, language);

        if (entry) {
            url = url + '/' + createSlug(entry.title, entry.id);

        }

        return url.replace(/\/+$/, '');
    };
}

export function useTranslatedBlogPath(lang) {
    return useTranslatedCanonicalPath(lang, canonicalBlogUrl);
}

export function useTranslatedProjectsPath(lang) {
    return useTranslatedCanonicalPath(lang, canonicalProjectsUrl);
}

export function useTranslatedAboutPath(lang) {
    const path = useTranslatedPath(lang);

    return function(entry, language = lang) {
        let url = path('/about', language);

        return url.replace(/\/+$/, '');
    };
}
