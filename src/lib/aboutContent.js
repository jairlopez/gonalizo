import { generateGetStaticPathsForSinglePage } from "./blogContent"
import { useTranslatedAboutPath } from "./i18n/utils.js"

export function generateGetStaticPathsForSingleAboutPage(language) {
    const aboutPath = useTranslatedAboutPath(language);

    return generateGetStaticPathsForSinglePage(language, 'about', aboutPath);
}
