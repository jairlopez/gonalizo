import { getCollection } from "astro:content";

export async function getFilteredCollection(collectionName, language, sorted, filteredOutCb) {
    const regex = new RegExp(`^${language}/`);
    const entries = await getCollection(collectionName);

    const filteredEntries = [];

    for (const entry of entries) {
        let includeEntry = import.meta.env.DEV || entry.data.draft !== true;

        if (regex.test(entry.id)) {
            if (includeEntry) {
                filteredEntries.push(entry);
            }
        } else if (includeEntry && typeof filteredOutCb === 'function') {
            filteredOutCb(entry);
        }
    }

    if (sorted) {
        filteredEntries.sort((a, b) => {
            // For now dates are compared as strings.

            if (a.data.pubDate === b.data.pubDate) {
                return 0;
            }

            if (b.data.pubDate > a.data.pubDate) {
                return 1;
            }

            return -1;
        })
    }

    return filteredEntries;
}
