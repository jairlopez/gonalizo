import { generateGetFunction } from '../../lib/rssContent';

const GET = await generateGetFunction();

export { GET };
