import { getCollection } from 'astro:content';

/**
* Entry from content collection.
*
* @typedef {Object} CollectionEntry
*
* @property {string} id - Entry ID.
*
* @property {object} data - Frontmatter settings.
*
* @property {string} data.title - Entry title.
*
* @property {string} data.description - Entry description.
*
* @property {string} data.pubDate - Entry publication date as a string in the
* format "YYYY-MM-DD".
*
* @property {string} data.heroImage - Path of the image shown in both
* HorizontalCard components and single pages. It is recommended to use 1470x730
* WEBP images.
*
* @property {string[]} data.tags - (Only available for blog posts) Tags of the
* blog post.
*
* @property {boolean} [data.draft] - Specifies whether a given content entry is
* a draft and (if so) hides it in production environment.
*
*/


/**
 * Get resume sections information from several content collections.
 */
export class Source {
    #htmlLang;
    #regex;
    #namespace

    /**
     * Create resume source.
     *
     * @param {string} htmlLang - Resume language. Usually it is `en` or `es`.
     *
     * @param {string} namespace - Common name used as prefix in resume
     * collections.
     */
    constructor(htmlLang, namespace) {
        this.#htmlLang = htmlLang;
        this.#namespace = namespace;
        this.#regex = new RegExp('^' + htmlLang + '/');
    }

    /**
     * Filter content collection by property `propertyName` and by language,
     * returning the first occurence. It takes into account collections whose
     * names begins with pobject roperty `#namespace` and ends with `collectionName`.
     *
     * @async
     * @private
     *
     * @param {string} collectionName - Resume section name.
     *
     * @param {string} propertyName - Collection's property name to filter by.
     *
     * @returns {Promise<CollectionEntry | undefined>} A collection entry (when any).
     */
    async #getSingleCollectionEntry(collectionName, propertyName) {
        const collection = await getCollection(this.#namespace + '' + collectionName);

        let result;

        for (const currentItem of collection) {
            if (this.#regex.test(currentItem[propertyName])) {
                result = currentItem;
                break;
            }
        }

        return result;
    }

    /**
     * Get the language used for filtering content collection entities.
     *
     * return {string}
     */
    get htmlLang() {
        return this.#htmlLang;
    }


    /**
     * Filter content collection by property `propertyName` and by language,
     * returning an array of occurences. It takes into account collections whose
     * names begins with object property `#namespace` and ends with `collectionName`.
     *
     * @async
     * @private
     *
     * @param {string} collectionName - Resume section name.
     *
     * @param {string} propertyName - Collection's property name to filter by.
     *
     * @returns {Promise<CollectionEntry[] | undefined>} An array of collection entry (when any).
     */
    async #getCollectionEntries(collectionName, propertyName) {
        return await getCollection(
            this.#namespace + '' + collectionName,
            item => this.#regex.test(item[propertyName])
        );
    }

    /**
     * Filter out content collection entries when `hide` property is true.
     *
     * @private
     *
     * @param {CollectionEntry[]} collection - Resume section name.
     *
     * @returns {CollectionEntry[]} An array of filtered collection entries.
     */
    #filterByHide(collection) {
        return collection.filter((item) => item.data.hide === false);
    }

    /**
     * Sort content collection entries by `index` property.
     *
     * @private
     *
     * @param {CollectionEntry[]} collection - Resume section name.
     *
     * @returns {CollectionEntry[]} An array of sorted collection entries.
     */
    #sortEntriesCollection(collection) {
        return collection.sort((a, b) => a.data.index - b.data.index)
    }

    /**
     * Get content of Summary section.
     *
     * @async
     *
     * @returns {Promise<CollectionEntry[]>}
     */
    async getSummary() {
        return await this.#getSingleCollectionEntry('Summary', 'id');
    }

    /**
     * Get content of Skill section.
     *
     * @async
     *
     * @returns {Promise<CollectionEntry[]>}
     */
    async getSkills() {
        return await this.#getSingleCollectionEntry('Skills', 'id');
    }

    /**
     * Get content of Readings section.
     *
     * @async
     *
     * @returns {Promise<CollectionEntry[]>}
     */
    async getReadings() {
        return await this.#getSingleCollectionEntry('Readings', 'id');
    }

    /**
     * Get content of Header section.
     *
     * @async
     *
     * @returns {Promise<CollectionEntry[]>}
     */
    async getHeader() {
        return await this.#getSingleCollectionEntry('Header', 'id');
    }

    /**
     * Get content of Projects section.
     *
     * @async
     *
     * @returns {Promise<CollectionEntry[]>}
     */
    async getProjects() {
        let entries = await this.#getCollectionEntries('Projects', 'id');

        return this.#sortEntriesCollection(this.#filterByHide(entries));
    }

    /**
     * Get content of Educations section.
     *
     * @async
     *
     * @returns {Promise<CollectionEntry[]>}
     */
    async getEducations() {
        let entries = await this.#getCollectionEntries('Educations', 'id');

        return this.#sortEntriesCollection(this.#filterByHide(entries));
    }

    /**
     * Get content of Educations section.
     *
     * @async
     *
     * @returns {Promise<CollectionEntry[]>}
     */
    async getExperiences() {
        let entries = await this.#getCollectionEntries('Experiences', 'id');

        return this.#sortEntriesCollection(this.#filterByHide(entries));
    }
}
