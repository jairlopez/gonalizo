import { createMarkdownProcessor } from '@astrojs/markdown-remark'

const processor = await createMarkdownProcessor({});

/**
 * Render inline Markdown text into HTML. It is used for adding additional
 * formatted text about resume sections.
 *
 * @async
 *
 * @param {string} summary - string representing Markdown text.
 *
 * @returns {Promise<string>} A collection entry (when any).
 */
export async function renderSummary(summary) {
    if (summary) {
        return '';
    }

    // SOURCE:
    // https://github.com/astro-community/md/blob/77e3e012a9b1ed19598e3965a57f55054255296e/packages/md/lib/markdown.js
    const result = await processor.render(summary, null);

    if (result.code.indexOf('<p>') === 0 && result.code.indexOf('</p>') === result.code.length - 4) {
        return ': ' + new String(result.code.slice(3, -4));
    }

    return ': ' + new String(result.code);
}

/**
 * Create a human-readable string representing the time elapsed between two
 * dates; not in the mathematical sense but just like a rough estimation a
 * human would make; rounding days up to the nearest month.
 *
 *
 * @param {string} date1 - Start date in the format `YYYY-MM-DD` as a string.
 *
 * @param {string} date2 - End date in the format `YYYY-MM-DD` as a string.
 *
 * @param {string} language - Language used in the resulting string. Only `en`
 * and `es` are currenty supported.
 *
 *
 * @returns {string} A human-readable phrase tallying up the amount of time
 * elapsed.
 */
export function getDateDifference(date1, date2, language = 'en') {
    if (date1 > date2) {
        [date1, date2] = [date2, date1];
    }

    const startDate = date1.split('-').map(item => parseInt(item, 10));
    const endDate = date2.split('-').map(item => parseInt(item, 10));

    let years = endDate[0] - startDate[0];

    let months, days;

    // Edge cases:

    // 2024-03-31
    // 2025-02-28
    // 11 months

    // 2024-05-31
    // 2025-04-29
    // 11 months


    // 2024-04-30
    // 2025-05-28
    // 1 year and 28 days


    if (startDate[1] > endDate[1]) {
        years--;
        months = 12 - startDate[1] + endDate[1];
    } else {
        months = Math.abs(startDate[1] - endDate[1]);
    }


    if (startDate[2] === endDate[2]) {
        days = 0;
    } else if (startDate[2] > endDate[2]) {
        let endDay = daysInMonth(endDate[1], endDate[0]);

        if (endDay !== endDate[2] || startDate[2] <= endDay) {
            months--;

            let pivotMonth = endDate[1] - 1;
            let pivotYear = endDate[0];

            if (pivotMonth === 0) {
                pivotYear--;
                years--;
                pivotMonth = 12;
            }

            days = daysInMonth(pivotMonth, pivotYear) - startDate[2] + endDate[2];
        } else {
            days = 0;
        }
    } else {
        days = endDate[2] - startDate[2];
    }

    /*
    console.log(`Years: ${years}`);
    console.log(`Months: ${months}`);
    console.log(`Days: ${days}`);
    console.log('');
    */

    return getDateDifferencePhrase(years, months, days, language);
}

/**
 * Create a translated phrase representing the time elapsed between two dates.
 * rounding days up to the nearest month.  See function `getDateDifference`. It
 * assumes the parameters are valid.
 *
 *
 * @param {integer} years - Amounts of years.
 *
 * @param {integer} months - Amounts of months.
 *
 * @param {integer} days - Amounts of days.
 *
 *
 * @returns {string} A human-readable phrase tallying up the amount of time
 * elapsed.
 */
function getDateDifferencePhrase(years, months, days, language) {
    const lang = {
        en: {
            years: 'years',
            year: 'year',
            months: 'months',
            month: 'month',
            days: 'days',
            day: 'day',
            and: 'and',
        },
        es: {
            years: 'años',
            year: 'año',
            months: 'meses',
            month: 'mes',
            days: 'días',
            day: 'día',
            and: 'y',
        },
    };

    let result = '';

    if (years !== 0) {
        result = `${years} ${lang[language][years !== 1 ? 'years' : 'year']}`;

        if (days > 15) {
            months++;
        }

        if (months !== 0) {
            result += ` ${lang[language]['and']} ${months} ${lang[language][months !== 1 ? 'months' : 'month']}`;
        }
    } else {
        if (days > 15) {
            months++;
        }

        result += `${months} ${lang[language][months !== 1 ? 'months' : 'month']}`;
    }

    return result;
}

/**
 * Calculate the number of days available in a given month.
 *
 *
 * @param {integer} month - The number of the month, starting at 1 for January.
 *
 * @param {integer} year - Year subject to the calculation, in order to take
 * leap years into account.
 *
 *
 * @returns {integer}
 */
function daysInMonth(month, year) {
    switch(month) {
        case 1:
        case 3:
        case 5:
        case 6:
        case 8:
        case 10:
        case 12:
            return 31;
        case 2:
            // SOURCE: https://github.com/astro-community/md/blob/77e3e012a9b1ed19598e3965a57f55054255296e/packages/md/lib/markdown.js
            //
            // Leap year occurs in each year that is a multiple of 4,
            // except for years evenly divisible by 100 but not by 400.

            if (year % 4 === 0 && !(year % 100 === 0 && !(year % 400 === 0))) {
                return 29;
            }

            return 28;
        default:
            return 30;
    }
}
