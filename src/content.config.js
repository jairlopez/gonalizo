import { defineCollection, z } from 'astro:content';
import { glob } from 'astro/loaders';

const projects = defineCollection({
    loader: glob({ pattern: "**/*.{md,mdx}", base: "./src/content/projects" }),
    schema: z.object({
        title: z.string(),
        description: z.string(),
        //pubDate: z.coerce.date(),
        pubDate: z.string(),
        updatedDate: z.string().optional(),
        heroImage: z.string().optional(),
        badge: z.string().optional(),
    })
});

const about = defineCollection({
    loader: glob({ pattern: "**/*.md", base: "./src/content/about" }),
    schema: z.object({
        title: z.string(),
        heroImage: z.string().optional(),
    })
});

const blog = defineCollection({
    loader: glob({ pattern: "**/*.{md,mdx}", base: "./src/content/blog" }),
    schema: z.object({
        title: z.string(),
        description: z.string(),
        //pubDate: z.coerce.date(),
        pubDate: z.string(),
        updatedDate: z.string().optional(),
        heroImage: z.string().optional(),
        badge: z.string().optional(),
        tags: z.array(z.string()).refine(items => new Set(items).size === items.length, {
            message: 'tags must be unique',
        }).optional()
    })
});

const translations = defineCollection({
    loader: glob({ pattern: '**/*.yml', base: './src/content/translations'}),
});

const resumeSummary = defineCollection({
    loader: glob({ pattern: '**/*.md', base: './src/content/resume/summary'}),
    schema: z.object({
        title: z.string().optional(),
    }),
});

const resumeSkills = defineCollection({
    loader: glob({ pattern: '**/*.yml', base: './src/content/resume/skills'}),
});

const resumeProjects = defineCollection({
    loader: glob({ pattern: '**/*.md', base: './src/content/resume/projects'}),
    schema: z.object({
        title: z.string(),
        hide: z.boolean(),
        index: z.number(),
    }),
});

const resumeHeadings = defineCollection({
    loader: glob({ pattern: '**/*.yml', base: './src/content/resume/headings'}),
});

const resumeHeader = defineCollection({
    loader: glob({ pattern: '**/*.yml', base: './src/content/resume/header'}),
});

const resumeExperiences = defineCollection({
    loader: glob({ pattern: '**/*.md', base: './src/content/resume/experiences'}),
});

const resumeEducations = defineCollection({
    loader: glob({ pattern: '**/*.md', base: './src/content/resume/educations'}),
});

const resumeReadings = defineCollection({
    loader: glob({ pattern: '**/*.yml', base: './src/content/resume/readings'}),
});


export const collections = {
    about,
    resumeSummary,
    resumeSkills,
    resumeProjects,
    projects,
    blog: blog,
    translations,
    resumeHeadings,
    resumeHeader,
    resumeExperiences,
    resumeEducations,
    resumeReadings
};
