---
jobTitle: Desarrollador Web Full Stack
company: Delstone Services LTA
country: Reino Unido
remote: true
showJobMode: false
startDate: "2017-12-01" # YYYY-MM-DD
endDate: "2019-03-31"   # YYYY-MM-DD
websiteName: Islamiqate
websiteLink: "https://www.islamiqate.com/"
index: 4
hide: false
markdownSummary: ''
---
Islamiqate es una aplicación web responsiva basada en Question2Answer que
recopila el mejor contenido sobre el Islam, su civilización y su gente. Logros
clave:

- Transformé exitosamente el sitio web en un diseño responsivo, mejorando la
  experiencia del usuario en diferentes dispositivos.

- Integré componentes front-end y back-end, asegurando un flujo de datos y
  funcionalidad sin problemas.

- Identifiqué y resolví vulnerabilidades de seguridad críticas, protegiendo los
  datos del usuario y la integridad del sistema.

- Implementé nuevas funciones como cierre de preguntas, modificación de autoría
  de publicaciones, feeds de usuarios personalizados, galerías de imágenes,
  creación de nuevos temas, actualizaciones de imágenes y sugerencias
  informativas, mejorando el engagement del usuario y la funcionalidad de la
  plataforma.

- Optimicé los scripts de despliegue para actualizaciones eficientes y
  confiables.
