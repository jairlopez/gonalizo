---
jobTitle: Desarrollador Full Stack
country: Estados Unidos
remote: true
showJobMode: false
startDate: "2019-05-01" # YYYY-MM-DD
endDate: "2022-05-01"   # YYYY-MM-DD
websiteName: ScieMce
websiteLink: "https://sciemce.com/"
index: 3
hide: false
markdownSummary: ''
---
Diseñé y desarrollé características centrales para ScieMce, una plataforma
colaborativa de estudio, mejorando significativamente la experiencia del
usuario e impulsando el engagement. Utilicé JavaScript puro, Question2Answer,
PHP y Manticore para construir una plataforma robusta y escalable.
Responsabilidades:

- Aumenté el engagement del usuario mediante el desarrollo de creación de
  flashcards, paquetes de estudio y funciones de paquetes de estudio
  colaborativos.

- Mejoré la relevancia de los resultados de búsqueda integrando el motor de
  búsqueda Manticore y optimizando los algoritmos de búsqueda.

- Mejoré la experiencia del usuario implementando modo oscuro/claro, flujos
  intuitivos de registro/inicio de sesión y un atractivo reproductor de
  flashcards.

- Integré exitosamente las API de PayPal Checkout y Subscription para permitir
  un procesamiento de pagos y gestión de suscripciones sin problemas.

- Superé las limitaciones de la arquitectura de Question2Answer implementando
  un motor de búsqueda personalizado para mejorar la precisión de los
  resultados de búsqueda, la colaboración del usuario y el procesamiento de
  pagos.
