---
jobTitle: Desarrollador Backend
company: Crea tu Imagen Digital
country: Perú
remote: true
showJobMode: false
startDate: "2024-06-01" # YYYY-MM-DD
endDate: "2024-09-01"   # YYYY-MM-DD
index: 1
hide: false
markdownSummary: ''
---
Optimicé con éxito el rendimiento de 8 microservicios REST API en un 26%
mediante una actualización estratégica de Spring Boot 2.6.7 a 3.2.3 y Java 21,
incluyendo la actualización de Oracle 11 a Oracle 19 y la documentación hecha
en Swagger. Esta mejora contribuyó directamente a la reducción de costos y al
aumento de la disponibilidad del sistema de entretenimiento en línea.
