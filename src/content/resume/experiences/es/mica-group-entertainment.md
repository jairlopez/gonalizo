---
jobTitle: Desarrollador Backend
country: Venezuela
company: MicaGroup Entertainment C.A.
remote: true
showJobMode: false
startDate: "2022-06-20" # YYYY-MM-DD
endDate: "2024-04-15"   # YYYY-MM-DD
#websiteName: Micasino.com
#websiteLink: "https://micasino.com/"
index: 2
hide: false
markdownSummary: ''
---
Desarrollé y mantuve un conjunto de tres aplicaciones web basadas en Laravel,
incluyendo un sitio web principal, una plataforma de análisis de apuestas y una
API, para apoyar a un casino online multinacional.

- Optimicé el rendimiento de la base de datos y racionalicé las operaciones
  para impulsar el crecimiento del negocio.

- Reduje el tiempo de resolución de tickets de soporte en un 54%, de un
  promedio de 33 minutos a 15 minutos, mediante la optimización de consultas de
  base de datos PostgreSQL y la conversión de informes no responsivos con
  millones de entradas en informes paginados y eficientes del lado del
  servidor.

- Aceleré la generación de informes en un 75% (de 5 segundos a 1 segundo en
  promedio) al migrar la generación de informes de Excel al backend, liberando
  recursos del navegador y reduciendo los tiempos de espera del usuario.

- Mejoré la legibilidad de los informes incorporando el formato faltante y
  visualizaciones de datos, mejorando la claridad y usabilidad de los datos.

- Migré la internacionalización de la dependencia `zerospam/laravel-gettext` al
  mecanismo integrado de Laravel, simplificando el proceso de localización y
  mejorando la gestión de traducciones.

- Desarrollé un componente Blade utilizando DataTable para crear tablas de
  informes interactivas y eficientes, mejorando la legibilidad del código y la
  reutilización, y mejorando la experiencia del usuario y las capacidades de
  análisis de datos.

- Amplié la funcionalidad del CRM agregando características para operaciones de
  servicio al cliente, aumentando la eficiencia y reduciendo el tiempo de
  resolución de tickets de soporte.

- Actualicé el stack de las aplicaciones migrando de PHP 7 a PHP 8.3 y de
  Laravel 8 a Laravel 10, mejorando la seguridad y el rendimiento de la
  aplicación.

- Implementé la replicación en streaming de PostgreSQL para deletar las
  operaciones de solo lectura a un servidor adicional, mejorando el rendimiento
  y la escalabilidad de la base de datos.

