---
jobTitle: Full Stack Web Developer
country: United States
remote: true
showJobMode: false
startDate: "2019-05-01" # YYYY-MM-DD
endDate: "2022-05-01"   # YYYY-MM-DD
websiteName: ScieMce
websiteLink: "https://sciemce.com/"
index: 3
hide: false
markdownSummary: ''
---
Designed and developed core features for ScieMce, a collaborative study
platform, significantly enhancing user experience and driving engagement.
Leveraged vanilla JavaScript, Question2Answer, PHP, and Manticore to build a
robust and scalable platform. Responsibilities:

- Increased user engagement through the development of flashcard creation,
  study packs, and collaborative study pack features

- Improved search results relevance integrating Manticore search engine and
  optimizing search algorithms

- Enhanced user experience by implementing dark/light mode, intuitive
  sign-up/login flows, and engaging flashcard player

- Successfully integrated PayPal Checkout and Subscription APIs to enable
  seamless payment processing and subscription management

- Overcame limitations of Question2Answer's architecture by implementing a
  custom search engine to improve search results accuracy, user collaboration,
  and payment processing
