---
jobTitle: Full Stack Web Developer
company: Delstone Services LTA
country: United Kingdom
remote: true
showJobMode: false
startDate: "2017-12-01" # YYYY-MM-DD
endDate: "2019-03-31"   # YYYY-MM-DD
websiteName: Islamiqate
websiteLink: "https://www.islamiqate.com/"
index: 4
hide: false
markdownSummary: ''
---
Islamiqate is a Question2Answer responsive web application that crowdsources
the best content on Islam, its civilization, and its people. Key achievements:

- Successfully transformed the website into a responsive design, enhancing user
  experience across different devices

- Integrated front-end and back-end components, ensuring seamless data flow and
  functionality

- Identified and resolved critical security vulnerabilities, protecting user
  data and system integrity

- Implemented new features including question closing, post authorship
  modification, custom user feeds, image galleries, new topic creation, image
  updates, and informative tooltips, improving user engagement and platform
  functionality

- Optimized deployment scripts for efficient and reliable updates
