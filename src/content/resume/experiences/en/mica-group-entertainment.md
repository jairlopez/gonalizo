---
jobTitle: Backend Developer
country: Venezuela
company: MicaGroup Entertainment C.A.
remote: true
showJobMode: false
startDate: "2022-06-20" # YYYY-MM-DD
endDate: "2024-04-15"   # YYYY-MM-DD
#websiteName: Micasino.com
#websiteLink: "https://micasino.com/"
index: 2
hide: false
markdownSummary: ''
---
Developed and maintained a suite of three Laravel-based web applications,
including a main website, a betting analytics platform, and an API, to support
a multinational online casino.

- Optimized database performance and streamlined operations to drive business
  growth.

- Decreased support ticket resolution time by 54% from an average of 33 minutes
  to 15 minutes by optimized PostgreSQL database queries and converting
  unresponsive reports with millions of entries into efficient, server-side
  paginated reports

- Accelerated report generation by 75% (from 5 second to 1 second in average)
  by migrating Excel report generation to the backend, freeing up browser
  resources and reducing user wait times

- Enhanced report readability by incorporating missing formatting and data
  visualizations, improving data clarity and usability

- Migrated internationalization from `zerospam/laravel-gettext` to Laravel's
  built-in mechanism, streamlining the localization process and improving
  translation management

- Develop a Blade component using DataTable to create interactive and efficient
  report tables, improving code readability and reusability, and improving user
  experience, data analysis capabilities

- Expanded betting analytics platform functionality by adding features for
  customer service operations, increasing efficiency and reducing support
  ticket resolution time

- Upgraded application stack by migrating from PHP 7 to PHP 8.3 and Laravel 8
  to Laravel 10, improving application security and performance

- Implemented PostgreSQL streaming replication to offload read-only operations,
  enhancing database performance and scalability
