---
jobTitle: Backend Developer
company: Crea tu Imagen Digital
country: Perú
remote: true
showJobMode: false
startDate: "2024-06-01" # YYYY-MM-DD
endDate: "2024-09-01"   # YYYY-MM-DD
index: 1
hide: false
markdownSummary: ''
---
Successfully optimized the performance of 8 REST API microservices by 26%
through a strategic upgrade from Spring Boot 2.6.7 to 3.2.3 and Java 21,
including database update from Oracle 11 to Oracle 19 and the Swagger
documentation. This enhancement directly contributed to cost savings and
increased system availability.
