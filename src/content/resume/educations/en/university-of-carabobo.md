---
degree: B.S. in Computer Science
school: Universidad de Carabobo
country: Venezuela
startDate: "2024-04-27" # YYYY-MM-DD
endDate: "2025-05-28"   # YYYY-MM-DD
showDates: false
index: 1
hide: false
markdownSummary: ''
---
- Achieved third-highest GPA in the Computer Science department in the first
  year of study. Maintained a strong academic record throughout the program

- Received top honors in the Web Development course (with a grade of 19 out of
  20), inspiring my career in software development

- Relevant coursework included Data Structures, Algorithms, Database Systems,
  and Web Development

- Degree completion extended due to multiple university strikes

- Demonstrated exceptional time management and perseverance by balancing
  full-time work and advanced English studies
