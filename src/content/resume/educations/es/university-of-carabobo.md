---
degree: Licenciado en Computación
school: Universidad de Carabobo
country: Venezuela
startDate: "2024-04-27" # YYYY-MM-DD
endDate: "2025-05-28"   # YYYY-MM-DD
index: 1
hide: false
markdownSummary: ''
---
- Logré el tercer lugar en promedio de calificaciones más alto del departamento
  de Ciencias de la Computación en el primer año de estudio. Mantuve un sólido
  rendimiento académico durante todo el programa.

- Recibí los más altos honores en el curso de Desarrollo Web (con una calificación de 19 sobre 20), lo que inspiró mi carrera en desarrollo de software.

- Las materias relevantes incluyeron Estructuras de Datos, Algoritmos, Sistemas de Bases de Datos y Desarrollo Web.

- La finalización de la carrera se extendió debido a múltiples huelgas universitarias.

- Demostré una excepcional gestión del tiempo y perseverancia al equilibrar el trabajo a tiempo completo y los estudios avanzados de inglés.
