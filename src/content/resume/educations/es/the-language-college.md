---
degree: Curso avanzado de Inglés
school: The Language College
country: Venezuela
startDate: "2013-04-01" # YYYY-MM-DD
endDate: "2015-12-31"   # YYYY-MM-DD
index: 2
hide: false
markdownSummary: '[Ver video sobre mi (18 segundos)](https://www.youtube.com/watch?v=22k7GQDn_eE).'
---
- Completé un programa integral de inglés, abarcando niveles básico, intermedio
  y avanzado, totalizando 325 horas académicas.

- Alcancé un sobresaliente promedio de 97/100, demostrando una excepcional
  competencia lingüística.

- Desarrollé una avanzada competencia en lectura, escritura, habla y
  comprensión auditiva, esencial para una efectiva colaboración en entornos de
  trabajo internacionales.
