---
title: 'About Me'
heroImage: "/img/about/motto-en.webp"
---

Hello, my name is Jair López. I'm a full-stack web developer—someone with the
experience and knowledge to integrate various pieces of code into functional
web applications that solve real-world business problems.

While a full-stack developer's primary role is integration, I can also dive
deep into any part of the stack as needed. My ultimate goal is to leverage
these technologies to build web applications that deliver value and help you
achieve your business objectives.

I’ve been working as a full-stack web developer since 2015 on projects
requiring expertise in:

- **Linux, Apache, and Docker:** I'm proficient in scripting with Bash, sed, and
  awk, as well as using Git for version control. I use Arch Linux, so I'm
  comfortable setting up websites, installing LAMP servers, and accessing
  remote servers via SSH, SFTP, and rsync.

- **MySQL and PostgreSQL:** I can create and optimize SQL queries for CRUD
  (Create, Read, Update, Delete) operations.

- **PHP:** I work with both vanilla PHP and projects built on frameworks like
  Laravel, CodeIgniter, and the Question2Answer platform.

- **JavaScript, jQuery, React, and Electron:** I use these technologies to add
  and modify dynamic front-end functionality, including debugging and improving
  existing jQuery code

- **HTML and CSS:** I design websites from scratch and modify existing designs,
  using JavaScript, jQuery, Bootstrap, and Tailwind CSS.

In the tech world, continuous learning is essential. We're constantly
researching and adapting to new technologies and challenges. Whether it's
learning Rust, Ruby on Rails, Python or TensorFlow—or even developing our
own solutions—we embrace the opportunity to solve problems.

This constant learning isn't a burden; it's what drives us. We're passionate
about what we do. Change isn't about working longer hours or being more
aggressive; it's about expanding our skill sets and becoming more effective,
resilient, and capable professionals. It's about staying happy and engaged in
our work.

I believe my skills and experience would be valuable assets to any project
requiring both back-end and front-end expertise.

Thank you for your time.
