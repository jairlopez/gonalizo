---
title: 'Acerca de mí'
heroImage: "/img/about/motto-es.webp"
---

Hola, mi nombre es Jair López. Soy un desarrollador web full-stack, alguien con
la experiencia y el conocimiento para integrar varias piezas de código en
aplicaciones web funcionales que resuelven problemas empresariales del mundo
real.

Si bien el rol principal de un desarrollador full-stack es la integración,
también puedo profundizar en cualquier parte del stack según sea necesario. Mi
objetivo final es aprovechar estas tecnologías para construir aplicaciones web
que entreguen valor y te ayuden a alcanzar tus objetivos empresariales.

He estado trabajando como desarrollador web full-stack desde 2015 en proyectos
que requieren experiencia en:

- **Linux, Apache y Docker:** Soy competente en scripting con Bash, sed y awk,
  así como en el uso de Git para el control de versiones. Uso Arch Linux, así
  que me siento cómodo configurando sitios web, instalando servidores LAMP y
  accediendo a servidores remotos a través de SSH, SFTP y rsync.

- **MySQL y PostgreSQL:** Puedo crear y optimizar consultas SQL para
  operaciones CRUD (Crear, Leer, Actualizar, Borrar).

- **PHP:** Trabajo tanto con PHP puro como con proyectos construidos sobre
  frameworks como Laravel, CodeIgniter y la plataforma Question2Answer.

- **JavaScript, jQuery, React y Electron:** Utilizo estas tecnologías para
  agregar y modificar la funcionalidad dinámica del front-end, incluyendo la
  depuración y la mejora del código jQuery existente.

- **HTML y CSS:** Diseño sitios web desde cero y modifico diseños existentes,
  utilizando JavaScript, jQuery, Bootstrap y Tailwind CSS.

En el mundo de la tecnología, el aprendizaje continuo es esencial. Estamos
constantemente investigando y adaptándonos a nuevas tecnologías y desafíos. Ya
sea aprendiendo Rust, Ruby on Rails, Python, TensorFlow, o incluso
desarrollando nuestras propias soluciones, aprovechamos la oportunidad de
resolver problemas.

Este aprendizaje constante no es una carga; es lo que nos impulsa. Nos apasiona
lo que hacemos. El cambio no se trata de trabajar más horas o ser más
agresivos; se trata de expandir nuestros conjuntos de habilidades y
convertirnos en profesionales más efectivos, resilientes y capaces. Se trata de
mantenernos felices y comprometidos con nuestro trabajo.

Creo que mis habilidades y experiencia serían activos valiosos para cualquier
proyecto que requiera experiencia tanto en back-end como en front-end.

Gracias por su tiempo.
