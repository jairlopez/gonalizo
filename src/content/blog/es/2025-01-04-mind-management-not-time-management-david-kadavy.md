---
title: Mind Management, Not Time Management por David Kadavy
description: Aprende cómo maximizar tu energía mental para mantenerte productivo en un mundo rápidamente cambiante y caótico.
pubDate: "2025-01-04"
heroImage: "/img/blog/2025-01-04/book-cover-picture.webp"
tags: ['book-review']
draft: true
---

Contenido aquí.
