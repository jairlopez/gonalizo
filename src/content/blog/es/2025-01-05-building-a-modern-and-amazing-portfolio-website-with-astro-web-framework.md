---
title: Construye un Sitio Web de Portafolio Moderno y Fascinante Con Astro Web Framework
description: Descubre cómo tomar ventaja de las bondades de Astro para transformar un tema básico en un portafolio profesional.
pubDate: "2025-01-05"
heroImage: "/img/blog/2025-01-05/astro-cover.webp"
tags: ['astro']
draft: true
---

Contenido aquí.
