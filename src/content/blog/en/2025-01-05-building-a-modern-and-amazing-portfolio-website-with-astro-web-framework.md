---
title: Building a Modern and Amazing Portfolio Website with Astro Web Framework
description: Discover how I took advantage of Astro features and turned an basic theme into a professional-grade portfolio.
pubDate: "2025-01-05"
heroImage: "/img/blog/2025-01-05/astro-cover.webp"
tags: ['astro']
draft: true
---

Content here.
