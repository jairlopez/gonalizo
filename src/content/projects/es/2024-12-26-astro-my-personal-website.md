---
title: 'Gonalizo: Mi sitio web de portafolio'
description: He construido un portafolio moderno y ágil para exhibir mi recorrido profesional y demostrar cómo empleo tecnologías de punta como Astro, Tailwind y daisyUI para crear soluciones impactantes.
pubDate: "2024-12-26"
heroImage: "/img/projects/2024-12-26/astro-portfolio.webp"
---

El dicho “Una imagen vale más que mil palabras” significa que las imágenes
pueden contar una historia de manera más efectiva que solo las palabras. Es
decir, las representaciones visuales pueden tener un impacto poderoso.

Si bien incluso los mejores currículos solo pueden contener una pequeña
fracción de esas “mil palabras”, un sitio web de portafolio es la forma ideal
de mostrar mi trabajo: me permite incluir explicaciones detalladas, imágenes,
videos, animaciones y mucho más.

Un portafolio en línea también asegura que sea fácilmente descubrible por
clientes o empleadores potenciales, permitiéndoles encontrarme a través de
buscadores o al revisar las solicitudes de candidatos.

También quería que el sitio web reflejara mi personalidad y mis valores
fundamentales, que son la base de todas mis relaciones profesionales:
responsabilidad, simplicidad y funcionalidad.

Debido a que disfruto de los desafíos, decidí construirlo utilizando
tecnologías con las que no estaba familiarizado, esto me permitió aprender
nuevas habilidades mientras creaba el sitio.


## Tecnologías utilizadas

Estaba pasando por una etapa difícil mientras trabajaba en este proyecto, pero
perseveré y superé con éxito tanto los desafíos personales como los de
desarrollo. Puede que comparta más sobre esa experiencia en el futuro. Por
ahora, aquí están las tecnologías que utilicé:

- **HTML**, **CSS**, **JavaScript**: Para la estructura, el estilo y las
  funcionalidades dinámicas del portafolio.

- **TailwindCSS**, **Tailwind CSS Typography**, **daisyUI**: Para personalizar
  el tema de Astrofy a mis preferencias personales.

Este proyecto en ya está desplegado en
[www.gonalizo.com](https://www.gonalizo.com/es), y el código fuente (bajo la
Licencia MIT) está disponible en
[GitLab](https://gitlab.com/jairlopez/gonalizo).


## Características principales

- **Diseño y tecnología modernas:** Aprovecha las últimas tecnologías con un diseño
  responsivo y un tema moderno.

- **Contenido diverso:** Incluye secciones para proyectos, publicaciones de
  blog paginadas, un currículo y un _feed RSS_, todo ello manteniendo las
  mejores prácticas de SEO. El contenido se gestiona mediante imágenes,
  archivos Markdown y YAML.

- **Currículo listo para imprimir:** Ofrece un currículo formal, simple y
  minimalista (disponible en inglés y español) listo para imprimir y
  se puede convertir fácilmente a un currículo en PDF.

- **Transiciones de página fluidas:** Utiliza la nueva API del navegador [View
  Transitions](https://developer.chrome.com/docs/web-platform/view-transitions/)
  para actualizar el contenido de la página sin recargas completas,
  lo que proporciona animaciones suaves entre páginas.

- **Fácil cambio de idioma:** Permite a los usuarios cambiar entre las traducciones
  disponibles (inglés y español) haciendo clic en los botones de idioma en la
  barra lateral, debajo de mi foto de perfil.

- **Múltiples opciones de contacto:** Muestra botones de contacto en la parte
  inferior de la barra lateral, lo que proporciona a los visitantes varias
  formas de contactarme.


## Resultados e Impacto

Trabajé duro en este proyecto, anticipando que tomaría muchos meses ver
resultados tangibles. Para mi sorpresa, el sitio web ya está funcionando bien
en el posicionamiento web, aunque todavía hay margen de mejora.

![Resultados del posicionamiento en buscadores para Gonalizo](/img/projects/2024-12-26/search-results-for-gonalizo-keyword.webp)

El sitio también está atrayendo tráfico orgánico, incluyendo cuatro posibles
contactos de clientes. Espero que este tráfico aumente a medida que continúe
agregando más contenido.

![Consumo de ancho de banda del sitio web](/img/projects/2024-12-26/bandwidth-consumption-december-2024.webp)

¡Esto es solo el comienzo! Ver el sitio web de portafolio terminado en mi
teléfono y notar que está generando tráfico es increíblemente gratificante. Me
proporciona una inmensa alegría, satisfacción e inspiración para este y futuros
proyectos.


## Lecciones Aprendidas

Como un creador apasionado, ver un proyecto desplegado siempre es un sueño
hecho realidad, especialmente cuando implica tanto aprendizaje:

- **Habilidades de desarrollo Front-End:** Adquirí dominio con el framework web
  Astro y obtuve experiencia práctica con Tailwind CSS.

- **Alojamiento y despliegue web:** Obtuve experiencia práctica desplegando un
  sitio web con un proveedor de hosting.

- **Fundamentos de administración de servidores:** Repasé los fundamentos de
  administración de sistemas creando scripts de despliegue y monitoreando los
  registros del servidor web.


## Colaboración

Si encuentras algún problema o tienes sugerencias, no dudes en abrir un issue
(o reportar un error) en el [repositorio de este
proyecto](https://gitlab.com/jairlopez/gonalizo/). Me esfuerzo por la
excelencia en todos mis proyectos, y creo que el feedback constante es clave
para el éxito, así que les agradezco mucho cualquier opinión.

No olvides explorar mi sitio web de portafolio. Siempre estoy disponible para
ayudarte a materializar tus proyectos.
