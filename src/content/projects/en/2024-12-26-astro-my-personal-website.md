---
title: 'Gonalizo: My Portfolio Website'
description: I've built a modern, streamlined portfolio to showcase my professional journey and demonstrate how I use cutting-edge technologies like Astro, Tailwind, and daisyUI to create impactful solutions.
pubDate: "2024-12-26"
heroImage: "/img/projects/2024-12-26/astro-portfolio.webp"
---

The saying _"A picture is worth a thousand words"_ means that images can tell a
story more effectively than words alone. In other words, visual representations
can have a powerful impact.

While even the best resumes can only contain a small fraction of that "thousand
words," a portfolio website is the ideal way to showcase my work. It allows me
to include detailed explanations, images, videos, animations, and more.

An online portfolio also ensures I'm easily discoverable by potential clients
or employers, allowing them to find me through search engines or when reviewing
candidate applications.

I also wanted the website to reflect my personality and core values, which are
the foundation of all my professional relationships: responsibility,
simplicity, and functionality.

Because I enjoy challenges, I decided to build it using technologies I was
unfamiliar with, this allowed me to learn new skills while creating the site.


## Technologies used

I was going through a difficult period while working on this project, but I
persevered and successfully overcame both personal and development challenges.
I may share more about that experience in the future. In the meantime, here are
the technologies I used:

- **HTML**, **CSS**, **JavaScript**: For portfolio's  structure, styling
  and dynamic functionalities.
- **TailwindCSS**, **Tailwind CSS Typography**, **daisyUI**: For customizing
  _Astrofy theme_ to my personal preferences.

This project at [www.gonalizo.com](https://www.gonalizo.com) is already
deployed, and the source code (under the MIT License) is available on
[GitLab](https://gitlab.com/jairlopez/gonalizo).

## Key features

- **Modern Design and Technology:** Leverages the latest technologies with a
  responsive design and a modern theme

- **Comprehensive Content:** Includes sections for projects, paginated blog posts,
  a CV, and an RSS feed, all while maintaining SEO best practices. Content is
  managed using images, Markdown, and YAML files

- **Print-Friendly CV:** Offers a formal, simple, and minimalist CV (available in
  English and Spanish) that is printer-friendly and easily convertible to a PDF
  resume

- **Seamless Page Transitions:** Utilizes the new [View
  Transitions](https://developer.chrome.com/docs/web-platform/view-transitions/)
  browser API to update page content without full-page refreshes, providing
  smooth animations between pages.

- **Easy Language Switching:** Allows users to switch between available
  translations (English and Spanish) by clicking the language buttons in the
  sidebar, below my profile picture.

- **Multiple Contact Options:** Displays contact buttons at the bottom of the
  sidebar, providing visitors with various ways to get in touch.

## Results and Impact

I invested significant effort in this project, anticipating that it would take
many months to see tangible results. To my surprise, the website is already
performing well in search engine rankings, even though there's still room for
improvement.

![Search engine ranking results for Gonalizo](/img/projects/2024-12-26/search-results-for-gonalizo-keyword.webp)

The site is also attracting organic traffic, including four potential client
leads. I expect this traffic to increase as I continue to add more content.

![Website bandwidth consumption](/img/projects/2024-12-26/bandwidth-consumption-december-2024.webp)

This is just the beginning! Seeing the finished portfolio website on my phone
and realizing it's generating traffic is incredibly rewarding. It provides me
with immense joy, satisfaction, and inspiration for this and future projects.

## Lessons Learned

As a passionate creator, seeing a project deployed is always a dream come true,
especially when it involves so much learning:

- **Front-End Development Skills:** Gained proficiency with the Astro web
  framework and gained practical experience with Tailwind CSS

- **Web Hosting and Deployment:** Gained practical experience deploying a
  website with a hosting provider

- **Server Administration Fundamentals:** Refreshed my understanding of system
  administration fundamentals by creating deployment scripts and monitoring web
  server logs

## Collaboration

If you encounter any issues or have suggestions, please feel free to open an
issue in [this project's repository](https://gitlab.com/jairlopez/gonalizo/). I
strive for excellence in all my projects, and I believe frequent feedback is
essential for success, so I greatly appreciate any recommendations.

Don't forget to explore my portfolio website. I'm always available to help
bring your visions to life.
